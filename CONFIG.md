# Keyboard shortcuts for testing and calibration
- **0 - 2**: switch to other cameras (0 = first default camera on the operation system)
- **b / B**: change camera brightness
- **c / C**: change camera contrast
- **e / E**: change camera exposure
- **f / F**: change camera focus
- **s**: save camera settings in the config.ini (camera id, brightness, config, exposure, focus, camera crop)
- **Enter**: switch to fullscreen, remove the title bar and move to the top left of the screen
- **!**  : stretch to fullscreen
- **q**: quit the application (only for Windows)
- **u, i, o**: simulate button 1 2 3 for right(top) player
- **k, j, h**: simulate button 1 2 3 for left(bottom) player
- **v**: use a fake camera feed
- **V**: display a visual feedback of what the camera sees
- **Arrows**: adjust camera crop coordinates
  - Top: use Up/Down arrows
  - Left: use Left/Right arrows
  - Bottom: use Shift+Up/Down arrows
  - Right: use Shift+Left/Right arrows

# Settings
TangrIAm app uses a config.ini file to adjust various settings regarding:
- gameplay (duration of the game, ...)
- camera adjustments (focus, exposure, crop, ...)
- IA settings (model, ...)
- UI elements (text, placement...)

## Gameplay
- GAME_DURATION: duration of one game
- INACTIVITY_TIME_LIMIT: delay after which the application go back to the main menu

## Camera settings
- ID: Values are from 0 to N representing the camera ID. 0 is the default main camera. 
- FOCUS: 
- CONTRAST: 
- BRIGHTNESS: 
- EXPOSURE: 

## IA settings
- Model: name of the AI model
- PREDICT_PS: number of predictions per second
- LABELS: names of all possible labels in the AI model

## UI elements
- width / height : application size (should match screen resolution for fullscreen)
- mask : mask image for application decoration
- (all texts)
- (coordinates, font, size, color)
