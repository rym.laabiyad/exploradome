import numpy as np
import matplotlib.pyplot as plt

from src.infrastructure.file_reader import get_labelled_data
from src.form_classification.model import build_on_top_of_pretrained_model, train_model

import tensorflow as tf
from tensorflow.keras.applications import MobileNetV2
from tensorflow.keras.optimizers import Adam


def plot_history(training_history):
    acc = training_history.history['accuracy']
    val_acc = training_history.history['val_accuracy']

    loss = training_history.history['loss']
    val_loss = training_history.history['val_loss']

    plt.figure(figsize=(8, 8))
    plt.subplot(2, 1, 1)
    plt.plot(acc, label='Training Accuracy')
    plt.plot(val_acc, label='Validation Accuracy')
    plt.legend(loc='lower right')
    plt.ylabel('Accuracy')
    plt.ylim([min(plt.ylim()), 1])
    plt.title('Training and Validation Accuracy')

    plt.subplot(2, 1, 2)
    plt.plot(loss, label='Training Loss')
    plt.plot(val_loss, label='Validation Loss')
    plt.legend(loc='upper right')
    plt.ylabel('Cross Entropy')
    plt.ylim([0, 1.0])
    plt.title('Training and Validation Loss')
    plt.xlabel('epoch')
    plt.show()


if __name__ == '__main__':
    import configparser
    import os

    config = configparser.ConfigParser()
    config.read(config_file)

    image_width = int(config['AI']['IMAGE_WIDTH'])
    image_height = int(config['AI']['IMAGE_HEIGHT'])
    n_channels = int(config['AI']['CHANNELS'])

    seed = 420
    np.random.seed(seed)
    tf.random.set_seed(seed)

    train_ds, test_ds = get_labelled_data(config['AI']['DATA_PATH'], image_height, image_width, data_augmentation=True)

    '''class_names = train_ds.class_names
    print(class_names)
    n_classes = len(class_names)'''
    n_classes = 12

    model_path = os.path.join(config['AI']['MODEL_PATH'], f'mobilenetv2_{n_classes}labels_{n_channels}channels_contrasted')

    model = build_on_top_of_pretrained_model(MobileNetV2, n_classes, image_height, image_width, n_channels)
    opt = Adam(lr=1e-3, decay=1e-5)

    trained_model, history = train_model(model, train_ds, test_ds, model_path, n_classes, opt, epochs=30)

    plot_history(history)
