import time
from configparser import ConfigParser, SectionProxy

from PIL import Image
from dataclasses import dataclass
from typing import Tuple, List, Dict, Callable, Optional
from PIL import ImageTk
from tkinter import Canvas, Button, Label

from infrastructure.video_reader import VideoReader


@dataclass
class Side:
    is_left: bool
    name: str = ""
    width: int = None
    height: int = None

    angle: int = None

    text_button_x: int = None
    text_button_y: List[int] = None
    button_x: int = None
    button_y: List[int] = None
    button1: Button = None
    button2: Button = None
    button3: Button = None

    tk_mask: int = None
    language_page_mask: ImageTk.PhotoImage = None
    welcome_page_mask: ImageTk.PhotoImage = None
    border_mask: ImageTk.PhotoImage = None
    video_consignes_mask: ImageTk.PhotoImage = None
    video_IA_choice_mask: ImageTk.PhotoImage = None
    video_IA_mask: ImageTk.PhotoImage = None
    game_welcome_page_mask: ImageTk.PhotoImage = None
    solo_pre_game_page_mask: ImageTk.PhotoImage = None
    duo_pre_game_page_mask: ImageTk.PhotoImage = None
    game_page_mask: ImageTk.PhotoImage = None
    proposition_list_mask: ImageTk.PhotoImage = None
    credits_page_mask: ImageTk.PhotoImage = None
    dialogue_box_mask: ImageTk.PhotoImage = None
    time_elapsed_image_mask: ImageTk.PhotoImage = None

    countdown_images: List[ImageTk.PhotoImage] = None
    tk_countdown_image: int = None

    tk_video_label: Label = None
    tk_video_container: int = None
    video_reader: VideoReader = None

    preview_live_x: int = None
    preview_live_y: int = None
    preview_crop_x: int = None
    preview_crop_y: int = None
    preview_heatmap_x: int = None
    preview_heatmap_y: int = None

    barchart_x: int = None
    barchart_y: int = None
    barchart_label_x: int = None
    barchart_score_x: int = None

    arc_center_x: int = None
    arc_center_y: int = None
    arc_radius: int = None
    arc_label_gap_y: int = None
    tk_arc: int = None
    tk_arc_label: int = None
    tk_arc_score: int = None

    timer_x: int = None
    timer_y: int = None
    tk_timer_min: int = None
    tk_timer_sep: int = None
    tk_timer_sec: int = None

    countdown_x: int = None
    countdown_y: int = None

    solution_image_x: int = None
    solution_image_y: int = None

    solution_text_x: int = None
    solution_text_1_y: int = None
    solution_text_2_y: int = None

    dialogue_box_text_x: int = None
    dialogue_box_text_y: int = None

    first_proposition_x: int = None
    first_proposition_y: int = None

    highlighted_proposition_x: int = None
    highlighted_proposition_y: int = None

    third_proposition_x: int = None
    third_proposition_y: int = None

    premiere_proposition_text_x: int = None
    premiere_proposition_text_line1_y: int = None
    premiere_proposition_text_line2_y: int = None
    premiere_proposition_text_line3_y: int = None
    premiere_proposition_text_line4_y: int = None
    premiere_proposition_image_x: int = None
    premiere_proposition_image_y: int = None

    tk_play_area: int = None
    play_area_coordinates: Tuple[int, int, int, int] = None

    list_of_choices_x: int = None
    list_of_choices_y: int = None
    list_of_choices_factor: int = None
    highlight_width: int = None
    highlight_height: int = None
    highlight: int = None

    solution: ImageTk.PhotoImage = None
    final_prediction: str = None
    prediction_score: float = None

    player_ready: bool = False
    player_choice_done: bool = False
    bounding_box = None
    cropped = None
    cropped_side = None
    explanation = None
    tk_bounding_box = None
    tk_cropped_side = None
    tk_cropped = None
    tk_explanation = None
    idx_label: int = -1
    tk_label_prev = None
    tk_label_current = None
    tk_label_next = None

    last_button_command_time = time.time()
    button_commands = [None, None, None]


class LocalizedUIData:

    def __init__(self, lang, folder, ui_data):
        import configparser

        print('Init UI ' + lang + ' from ' + folder)
        config: ConfigParser = configparser.ConfigParser()
        config.read(folder + "ui.ini", "utf8")
        print('config=' + str(config))
        ui_config = config['UI']
        self.folder = folder
        self.language = ui_config['LANGUAGE']
        self.title_font = ui_config['TITLE_FONT']
        self.title_font_color = ui_config['TITLE_FONT_COLOR']
        self.title_font_size = int(ui_config['TITLE_FONT_SIZE'])

        self.video_consignes = self.folder + ui_config['VIDEO_CONSIGNES']
        self.video_IA_1 = self.folder + ui_config['VIDEO_IA_1']
        self.video_IA_2 = self.folder + ui_config['VIDEO_IA_2']
        self.welcome_page_mask = self.folder + ui_config['WELCOME_PAGE_MASK']
        self.video_consignes_mask = self.folder + ui_config['VIDEO_CONSIGNES_MASK']
        self.video_ia_choice_mask = self.folder + ui_config['VIDEO_IA_CHOICE_MASK']
        self.video_ia_mask = self.folder + ui_config['VIDEO_IA_MASK']
        self.game_welcome_page_mask = self.folder + ui_config['GAME_WELCOME_PAGE_MASK']
        self.solo_pre_game_page_mask = self.folder + ui_config['SOLO_PRE_GAME_PAGE_MASK']
        self.duo_pre_game_page_mask = self.folder + ui_config['DUO_PRE_GAME_PAGE_MASK']
        self.time_elapsed_image_mask = self.folder + ui_config['TIME_ELAPSED_IMAGE_MASK']
        self.game_page_mask = self.folder + ui_config['GAME_PAGE_MASK']
        self.proposition_list_mask = self.folder + ui_config['PROPOSITION_LIST_MASK']
        self.credits_page_mask = self.folder + ui_config['CREDITS_PAGE_MASK']

        self.welcome_page_ia_button = ui_config["WELCOME_PAGE_IA_BUTTON"]
        self.welcome_page_play_button = ui_config["WELCOME_PAGE_PLAY_BUTTON"]
        self.welcome_page_credits_button = ui_config["WELCOME_PAGE_CREDITS_BUTTON"]
        self.video_ia_1_button = ui_config["VIDEO_IA_1_BUTTON"]
        self.video_ia_2_button = ui_config["VIDEO_IA_2_BUTTON"]
        self.back_button = ui_config["BACK_BUTTON"]
        self.solo_button = ui_config["SOLO_BUTTON"]
        self.duo_button = ui_config["DUO_BUTTON"]
        self.yes_button = ui_config["YES_BUTTON"]
        self.return_welcome_button = ui_config["RETURN_WELCOME_BUTTON"]
        self.retry_button = ui_config["RETRY_BUTTON"]
        self.instructions_page_skip_button = ui_config["INSTRUCTIONS_PAGE_SKIP_BUTTON"]
        self.instructions_page_back_button = ui_config["INSTRUCTIONS_PAGE_BACK_BUTTON"]
        self.pre_game_page_play_button = ui_config["PRE_GAME_PAGE_PLAY_BUTTON"]
        self.pre_game_page_consignes_button = ui_config["PRE_GAME_PAGE_CONSIGNES_BUTTON"]
        self.failure_text = ui_config["FAILURE_TEXT"]
        self.wait_player_text = ui_config["WAIT_PLAYER_TEXT"]
        self.solution_display_text_line1 = ui_config["SOLUTION_DISPLAY_TEXT_LINE1"]
        self.solution_display_text_line2 = ui_config["SOLUTION_DISPLAY_TEXT_LINE2"]
        self.inactive_menu_text = ui_config["INACTIVE_MENU_TEXT"]
        self.display_solution_text_line1 = ui_config["DISPLAY_SOLUTION_TEXT_LINE1"]
        self.display_solution_text_line2 = ui_config["DISPLAY_SOLUTION_TEXT_LINE2"]
        self.popup_retry_text = ui_config["POPUP_RETRY_TEXT"]
        self.popup_menu_text = ui_config["POPUP_MENU_TEXT"]
        self.dialogue_box_text_font = ui_config["DIALOGUE_BOX_TEXT_FONT"]
        self.dialogue_box_text_font_size = ui_config["DIALOGUE_BOX_TEXT_FONT_SIZE"]
        self.premiere_proposition_text_font = ui_config["PREMIERE_PROPOSITION_TEXT_FONT"]
        self.premiere_proposition_text_font_size = ui_config["PREMIERE_PROPOSITION_TEXT_FONT_SIZE"]
        self.premiere_proposition_text_font_color = ui_config["PREMIERE_PROPOSITION_TEXT_FONT_COLOR"]
        self.premiere_proposition_label_font = ui_config["PREMIERE_PROPOSITION_LABEL_FONT"]
        self.premiere_proposition_label_font_size = ui_config["PREMIERE_PROPOSITION_LABEL_FONT_SIZE"]
        self.premiere_proposition_label_font_color = ui_config["PREMIERE_PROPOSITION_LABEL_FONT_COLOR"]

        self.waiting_for_master_player_choice_text = ui_config["WAITING_FOR_MASTER_PLAYER_CHOICE_TEXT"]
        self.premiere_proposition_text_line1 = ui_config["PREMIERE_PROPOSITION_TEXT_LINE1"]
        self.premiere_proposition_text_line2 = ui_config["PREMIERE_PROPOSITION_TEXT_LINE2"]
        self.premiere_proposition_text_line4 = ui_config["PREMIERE_PROPOSITION_TEXT_LINE4"]

        self.premiere_proposition_image_size_width = int(ui_config['PREMIERE_PROPOSITION_IMAGE_SIZE_WIDTH'])
        self.premiere_proposition_image_size_height = int(ui_config['PREMIERE_PROPOSITION_IMAGE_SIZE_HEIGHT'])

        self.preview_live_width = int(ui_config['PREVIEW_LIVE_WIDTH'])
        self.preview_live_height = int(int(ui_config['PREVIEW_LIVE_HEIGHT']))
        self.preview_crop_width = int(ui_config['PREVIEW_CROP_WIDTH'])
        self.preview_crop_height = int(int(ui_config['PREVIEW_CROP_HEIGHT']))
        self.preview_heatmap_width = int(ui_config['PREVIEW_HEATMAP_WIDTH'])
        self.preview_heatmap_height = int(int(ui_config['PREVIEW_HEATMAP_HEIGHT']))
        self.bar_width = int(ui_config['BAR_WIDTH'])
        self.bar_height = int(ui_config['BAR_HEIGHT'])
        self.barchart_y_gap = int(ui_config['BARCHART_Y_GAP'])
        self.prediction_colors = [color.strip() for color in ui_config['PREDICTION_COLORS'].split(',')]
        self.barchart_label_font = ui_config['BARCHART_LABEL_FONT']
        self.barchart_label_font_color = ui_config['BARCHART_LABEL_FONT_COLOR']
        self.barchart_label_font_size = int(ui_config['BARCHART_LABEL_FONT_SIZE'])
        self.barchart_score_font = ui_config['BARCHART_SCORE_FONT']
        self.barchart_score_font_color = ui_config['BARCHART_SCORE_FONT_COLOR']
        self.barchart_score_font_size = int(ui_config['BARCHART_SCORE_FONT_SIZE'])
        self.title_y_ratio = float(ui_config['TITLE_Y_RATIO'])
        self.timer_font = ui_config['TIMER_FONT']
        self.timer_font_color = ui_config['TIMER_FONT_COLOR']
        self.timer_font_size = int(ui_config['TIMER_FONT_SIZE'])
        self.playarea_left = int(ui_config['PLAYAREA_LEFT'])
        self.playarea_right = int(ui_config['PLAYAREA_RIGHT'])
        self.playarea_top = int(ui_config['PLAYAREA_TOP'])
        self.playarea_bottom = int(ui_config['PLAYAREA_BOTTOM'])
        self.playarea_border_color = ui_config['PLAYAREA_BORDER_COLOR']
        self.playarea_safety_distance = int(ui_config['PLAYAREA_SAFETY_DISTANCE'])
        self.button_width = 30
        self.button_height = 30
        self.button_font = ui_config['BUTTON_FONT']
        self.button_font_color = ui_config['BUTTON_FONT_COLOR']
        self.button_font_size = int(ui_config['BUTTON_FONT_SIZE'])
        self.arc_label_font_size = int(ui_config['ARC_LABEL_FONT_SIZE'])
        self.arc_score_font_size = int(ui_config['ARC_SCORE_FONT_SIZE'])
        self.arc_radius = int(ui_config['ARC_RADIUS'])

        self.left: Side = Side(is_left=True, name="left")
        self.right: Side = Side(is_left=False, name="right")

        self.side_width = ui_data.side_width
        self.side_height = ui_data.side_height
        self.left.width = self.side_width
        self.left.height = self.side_height
        self.right.width = self.side_width
        self.right.height = self.side_height

        self.left.angle = -90
        self.right.angle = 90

        self.video_IA_width = int(ui_config['VIDEO_IA_HEIGHT'])
        self.video_IA_height = int(ui_config['VIDEO_IA_WIDTH'])

        self.left.video_IA_x = self.side_width - int(ui_config['VIDEO_IA_Y'])
        self.left.video_IA_y = int(ui_config['VIDEO_IA_X'])

        self.right.video_IA_x = int(ui_config['VIDEO_IA_Y'])
        self.right.video_IA_y = self.side_height - int(ui_config['VIDEO_IA_X'])

        self.video_consignes_width = int(ui_config['VIDEO_CONSIGNES_HEIGHT'])
        self.video_consignes_height = int(ui_config['VIDEO_CONSIGNES_WIDTH'])

        self.left.video_consignes_x = self.side_width - int(ui_config['VIDEO_CONSIGNES_Y'])
        self.left.video_consignes_y = int(ui_config['VIDEO_CONSIGNES_X'])

        self.right.video_consignes_x = int(ui_config['VIDEO_CONSIGNES_Y'])
        self.right.video_consignes_y = self.side_height - int(ui_config['VIDEO_CONSIGNES_X'])

        self.left.preview_live_x = int(ui_config['PREVIEW_LIVE_X'])
        self.left.preview_live_y = self.side_width - int(ui_config['PREVIEW_LIVE_Y'])
        self.left.preview_crop_x = int(ui_config['PREVIEW_CROP_X'])
        self.left.preview_crop_y = self.side_width - int(ui_config['PREVIEW_CROP_Y'])
        self.left.preview_heatmap_x = int(ui_config['PREVIEW_HEATMAP_X'])
        self.left.preview_heatmap_y = self.side_width - int(ui_config['PREVIEW_HEATMAP_Y'])

        self.right.preview_live_x = self.side_height - int(ui_config['PREVIEW_LIVE_X'])
        self.right.preview_live_y = int(ui_config['PREVIEW_LIVE_Y'])
        self.right.preview_crop_x = self.side_height - int(ui_config['PREVIEW_CROP_X'])
        self.right.preview_crop_y = int(ui_config['PREVIEW_CROP_Y'])
        self.right.preview_heatmap_x = self.side_height - int(ui_config['PREVIEW_HEATMAP_X'])
        self.right.preview_heatmap_y = int(ui_config['PREVIEW_HEATMAP_Y'])

        self.left.barchart_x = int(ui_config['BARCHART_X'])
        self.left.barchart_y = self.side_width - int(ui_config['BARCHART_Y'])
        self.left.barchart_label_x = int(ui_config['BARCHART_LABEL_X'])
        self.left.barchart_score_x = int(ui_config['BARCHART_SCORE_X'])

        self.left.arc_center_x = int(ui_config['ARC_CENTER_X'])
        self.left.arc_center_y = self.side_width - int(ui_config['ARC_CENTER_Y'])
        self.left.arc_label_gap_y = - int(ui_config['ARC_LABEL_GAP_Y']) - self.arc_radius

        self.right.barchart_x = self.side_height - int(ui_config['BARCHART_X'])
        self.right.barchart_y = int(ui_config['BARCHART_Y'])
        self.right.barchart_label_x = self.side_height - int(ui_config['BARCHART_LABEL_X'])
        self.right.barchart_score_x = self.side_height - int(ui_config['BARCHART_SCORE_X'])

        self.right.arc_center_x = self.side_height - int(ui_config['ARC_CENTER_X'])
        self.right.arc_center_y = int(ui_config['ARC_CENTER_Y'])
        self.right.arc_label_gap_y = int(ui_config['ARC_LABEL_GAP_Y']) + self.arc_radius

        self.left.text_button_x = int(ui_config['TEXT_BUTTON_X'])
        self.left.button_x = self.left.text_button_x + self.button_height / 2 + 10
        self.right.text_button_x = self.side_height - int(ui_config['TEXT_BUTTON_X'])
        self.right.button_x = self.right.text_button_x - self.button_height / 2 - 10
        self.right.text_button_y = [int(y) for y in ui_config['TEXT_BUTTON_Y'].split(',')]
        self.left.text_button_y = [self.side_width - y for y in self.right.text_button_y]

        self.left.timer_x = int(ui_config['TIMER_X'])
        self.left.timer_y = self.side_width - int(ui_config['TIMER_Y'])
        self.right.timer_x = self.side_height - int(ui_config['TIMER_X'])
        self.right.timer_y = int(ui_config['TIMER_Y'])

        self.left.first_proposition_x = int(ui_config['FIRST_PROPOSITION_X'])
        self.left.first_proposition_y = self.side_width - int(ui_config['FIRST_PROPOSITION_Y'])
        self.right.first_proposition_x = self.side_height - int(ui_config['FIRST_PROPOSITION_X'])
        self.right.first_proposition_y = int(ui_config['FIRST_PROPOSITION_Y'])
        self.first_proposition_font = ui_config['FIRST_PROPOSITION_FONT']
        self.first_proposition_font_size = ui_config['FIRST_PROPOSITION_FONT_SIZE']
        self.first_proposition_font_color = ui_config['FIRST_PROPOSITION_FONT_COLOR']

        self.left.highlighted_proposition_x = int(ui_config['HIGHLIGHTED_PROPOSITION_X'])
        self.left.highlighted_proposition_y = self.side_width - int(ui_config['HIGHLIGHTED_PROPOSITION_Y'])
        self.right.highlighted_proposition_x = self.side_height - int(ui_config['HIGHLIGHTED_PROPOSITION_X'])
        self.right.highlighted_proposition_y = int(ui_config['HIGHLIGHTED_PROPOSITION_Y'])
        self.highlighted_proposition_font = ui_config['HIGHLIGHTED_PROPOSITION_FONT']
        self.highlighted_proposition_font_size = ui_config['HIGHLIGHTED_PROPOSITION_FONT_SIZE']
        self.highlighted_proposition_font_color = ui_config['HIGHLIGHTED_PROPOSITION_FONT_COLOR']

        self.left.third_proposition_x = int(ui_config['THIRD_PROPOSITION_X'])
        self.left.third_proposition_y = self.side_width - int(ui_config['THIRD_PROPOSITION_Y'])
        self.right.third_proposition_x = self.side_height - int(ui_config['THIRD_PROPOSITION_X'])
        self.right.third_proposition_y = int(ui_config['THIRD_PROPOSITION_Y'])
        self.third_proposition_font = ui_config['THIRD_PROPOSITION_FONT']
        self.third_proposition_font_size = ui_config['THIRD_PROPOSITION_FONT_SIZE']
        self.third_proposition_font_color = ui_config['THIRD_PROPOSITION_FONT_COLOR']

        self.left.countdown_x = self.side_width - int(ui_config['COUNTDOWN_Y'])
        self.left.countdown_y = int(ui_config['COUNTDOWN_X'])
        self.right.countdown_x = int(ui_config['COUNTDOWN_Y'])
        self.right.countdown_y = self.side_height - int(ui_config['COUNTDOWN_X'])

        self.left.solution_text_x = int(ui_config['SOLUTION_TEXT_X'])
        self.left.solution_text_1_y = self.side_width - int(ui_config['SOLUTION_TEXT_1_Y'])
        self.left.solution_text_2_y = self.side_width - int(ui_config['SOLUTION_TEXT_2_Y'])
        self.right.solution_text_x = self.side_height - int(ui_config['SOLUTION_TEXT_X'])
        self.right.solution_text_1_y = int(ui_config['SOLUTION_TEXT_1_Y'])
        self.right.solution_text_2_y = int(ui_config['SOLUTION_TEXT_2_Y'])
        self.solution_text_font = ui_config['SOLUTION_TEXT_FONT']
        self.solution_text_font_size = ui_config['SOLUTION_TEXT_FONT_SIZE']

        self.left.solution_image_x = int(ui_config['SOLUTION_IMAGE_X'])
        self.left.solution_image_y = self.side_width - int(ui_config['SOLUTION_IMAGE_Y'])
        self.right.solution_image_x = self.side_height - int(ui_config['SOLUTION_IMAGE_X'])
        self.right.solution_image_y = int(ui_config['SOLUTION_IMAGE_Y'])

        self.left.premiere_proposition_image_x = int(ui_config['PREMIERE_PROPOSITION_IMAGE_X'])
        self.left.premiere_proposition_image_y = self.side_width - int(
            ui_config['PREMIERE_PROPOSITION_IMAGE_Y'])

        self.left.premiere_proposition_text_x = int(ui_config['PREMIERE_PROPOSITION_TEXT_X'])
        self.left.premiere_proposition_text_line1_y = self.side_width - int(
            ui_config['PREMIERE_PROPOSITION_TEXT_LINE1_Y'])
        self.left.premiere_proposition_text_line2_y = self.side_width - int(
            ui_config['PREMIERE_PROPOSITION_TEXT_LINE2_Y'])
        self.left.premiere_proposition_text_line3_y = self.side_width - int(
            ui_config['PREMIERE_PROPOSITION_TEXT_LINE3_Y'])
        self.left.premiere_proposition_text_line4_y = self.side_width - int(
            ui_config['PREMIERE_PROPOSITION_TEXT_LINE4_Y'])

        self.right.premiere_proposition_text_x = self.side_height - int(
            ui_config['PREMIERE_PROPOSITION_TEXT_X'])
        self.right.premiere_proposition_text_line1_y = int(ui_config['PREMIERE_PROPOSITION_TEXT_LINE1_Y'])
        self.right.premiere_proposition_text_line2_y = int(ui_config['PREMIERE_PROPOSITION_TEXT_LINE2_Y'])
        self.right.premiere_proposition_text_line3_y = int(ui_config['PREMIERE_PROPOSITION_TEXT_LINE3_Y'])
        self.right.premiere_proposition_text_line4_y = int(ui_config['PREMIERE_PROPOSITION_TEXT_LINE4_Y'])
        self.right.premiere_proposition_image_x = self.side_height - int(ui_config[
                                                                             'PREMIERE_PROPOSITION_IMAGE_X'])
        self.right.premiere_proposition_image_y = int(ui_config['PREMIERE_PROPOSITION_IMAGE_Y'])

        self.left.dialogue_box_text_x = int(ui_config['DIALOGUE_BOX_TEXT_X'])
        self.left.dialogue_box_text_y = self.side_width - int(ui_config['DIALOGUE_BOX_TEXT_Y'])
        self.right.dialogue_box_text_x = self.side_height - int(ui_config['DIALOGUE_BOX_TEXT_X'])
        self.right.dialogue_box_text_y = int(ui_config['DIALOGUE_BOX_TEXT_Y'])

        self.left.countdown_images = []
        self.right.countdown_images = []

        self.left.list_of_choices_x = self.side_height - 100
        self.left.list_of_choices_y = self.side_width / 3
        self.left.list_of_choices_factor = -40
        self.left.highlight_width = int(ui_config['HIGHLIGHT_WIDTH'])
        self.left.highlight_height = -int(ui_config['HIGHLIGHT_HEIGHT'])
        self.right.list_of_choices_x = 0
        self.right.list_of_choices_y = 2 * self.side_width / 3
        self.right.list_of_choices_factor = 40
        self.right.highlight_width = -int(ui_config['HIGHLIGHT_WIDTH'])
        self.right.highlight_height = int(ui_config['HIGHLIGHT_HEIGHT'])

        self.left.play_area_coordinates = (self.playarea_bottom, self.playarea_left,
                                           self.side_width - self.playarea_top,
                                           self.side_height - self.playarea_right)

        self.right.play_area_coordinates = (self.playarea_top, self.playarea_right,
                                            self.side_width - self.playarea_bottom,
                                            self.side_height - self.playarea_left)

        self.labels: List[str] = [label.strip() for label in ui_config["LABELS"].split(",")]
        self.articles: List[str] = [article.strip() for article in ui_config["ARTICLES"].split(",")]


class UIData:
    def __init__(self, ui_config):
        print('Init UI...')
        self.folder = "../" + ui_config["FOLDER"] + "/"
        self.game_mode: str = ''
        self.master_side: str = ''
        self.languages = [lang.strip() for lang in ui_config['LANGUAGES'].split(',')]
        self.show_buttons = ui_config.getboolean("SHOW_BUTTONS")
        self.background_color = ui_config['BACKGROUND_COLOR']
        self.time_out_display_duration = float(ui_config['TIME_OUT_DISPLAY_DURATION'])
        self.fps = int(ui_config["FPS"])
        self.width = int(ui_config['WIDTH'])
        self.height = int(ui_config['HEIGHT'])
        self.side_width = int(int(ui_config['WIDTH']) / 2)
        self.side_height = int(ui_config['HEIGHT'])
        self.dialogue_box = self.folder + ui_config['DIALOGUE_BOX']
        self.border_left_mask = self.folder + ui_config['BORDER_LEFT_MASK']
        self.border_right_mask = self.folder + ui_config['BORDER_RIGHT_MASK']
        self.countdown_images = [(self.folder + i.strip()) for i in ui_config['COUNTDOWN_IMAGES'].split(',')]
        self.language_page_mask = self.folder + ui_config['LANGUAGE_PAGE_MASK']
        self.left_canvas: Canvas = None
        self.right_canvas: Canvas = None

        self.current_language = self.languages[0]
        self.mui: Dict[str, LocalizedUIData] = {}
        for lang in self.languages:
            self.mui[lang] = LocalizedUIData(lang, self.folder + lang + "/", self)
        self.l: LocalizedUIData = self.mui[self.current_language]

    def get_canvas(self, side:Side):
        if side.is_left:
            return self.left_canvas
        else:
            return self.right_canvas

    def set_lang(self, lang):
        print("Switching to " + lang)
        self.current_language = lang
        self.l = self.mui[lang]

    def get_label(self, tech_label, tech_labels) -> str:
        return self.l.labels[tech_labels.index(tech_label)]

    def get_article_label(self, tech_label, tech_labels) -> str:
        idx = tech_labels.index(tech_label)
        return self.l.articles[idx] + " " + self.l.labels[idx]

    def do_button_command(self, side: Side = None, button_id: int = 0, command=None):
        current_time = time.time()
        if side is None:
            side = self.l.left
        if current_time - side.last_button_command_time < 0.3:
            return
        side.last_button_command_time = current_time
        if command is not None:
            command()
            return
        if side is not None and side.button_commands[button_id] is not None:
            side.button_commands[button_id]()

    def current_side(self) -> Side:
        if self.game_mode == 'left':
            return self.l.left
        elif self.game_mode == 'right':
            return self.l.right

    def opposite_side(self) -> Side:
        if self.game_mode == 'left':
            return self.l.right
        elif self.game_mode == 'right':
            return self.l.left

    def load_masks(self):
        language_page_mask = Image.open(self.language_page_mask)
        self.l.left.language_page_mask = ImageTk.PhotoImage(image=language_page_mask)
        self.l.right.language_page_mask = ImageTk.PhotoImage(image=language_page_mask.rotate(180))

        welcome_page_mask = Image.open(self.l.welcome_page_mask)
        self.l.left.welcome_page_mask = ImageTk.PhotoImage(image=welcome_page_mask)
        self.l.right.welcome_page_mask = ImageTk.PhotoImage(image=welcome_page_mask.rotate(180))

        self.l.left.border_mask = ImageTk.PhotoImage(file=self.border_left_mask)
        self.l.right.border_mask = ImageTk.PhotoImage(file=self.border_right_mask)

        video_mask = Image.open(self.l.video_consignes_mask)
        self.l.left.video_consignes_mask = ImageTk.PhotoImage(image=video_mask)
        self.l.right.video_consignes_mask = ImageTk.PhotoImage(image=video_mask.rotate(180))

        video_ia_choice_mask = Image.open(self.l.video_ia_choice_mask)
        self.l.left.video_IA_choice_mask = ImageTk.PhotoImage(image=video_ia_choice_mask)
        self.l.right.video_IA_choice_mask = ImageTk.PhotoImage(image=video_ia_choice_mask.rotate(180))

        video_ia_mask = Image.open(self.l.video_ia_mask)
        self.l.left.video_IA_mask = ImageTk.PhotoImage(image=video_ia_mask)
        self.l.right.video_IA_mask = ImageTk.PhotoImage(image=video_ia_mask.rotate(180))

        game_welcome_page_mask = Image.open(self.l.game_welcome_page_mask)
        self.l.left.game_welcome_page_mask = ImageTk.PhotoImage(image=game_welcome_page_mask)
        self.l.right.game_welcome_page_mask = ImageTk.PhotoImage(image=game_welcome_page_mask.rotate(180))

        solo_pre_game_page_mask = Image.open(self.l.solo_pre_game_page_mask)
        self.l.left.solo_pre_game_page_mask = ImageTk.PhotoImage(image=solo_pre_game_page_mask)
        self.l.right.solo_pre_game_page_mask = ImageTk.PhotoImage(image=solo_pre_game_page_mask.rotate(180))

        duo_pre_game_page_mask = Image.open(self.l.duo_pre_game_page_mask)
        self.l.left.duo_pre_game_page_mask = ImageTk.PhotoImage(image=duo_pre_game_page_mask)
        self.l.right.duo_pre_game_page_mask = ImageTk.PhotoImage(image=duo_pre_game_page_mask.rotate(180))

        time_elapsed_image_mask = Image.open(self.l.time_elapsed_image_mask)
        self.l.left.time_elapsed_image_mask = ImageTk.PhotoImage(image=time_elapsed_image_mask)
        self.l.right.time_elapsed_image_mask = ImageTk.PhotoImage(image=time_elapsed_image_mask.rotate(180))

        game_page_mask = Image.open(self.l.game_page_mask)
        self.l.left.game_page_mask = ImageTk.PhotoImage(image=game_page_mask)
        self.l.right.game_page_mask = ImageTk.PhotoImage(image=game_page_mask.rotate(180))

        proposition_list_mask = Image.open(self.l.proposition_list_mask)
        self.l.left.proposition_list_mask = ImageTk.PhotoImage(image=proposition_list_mask)
        self.l.right.proposition_list_mask = ImageTk.PhotoImage(image=proposition_list_mask.rotate(180))

        credits_page_mask = Image.open(self.l.credits_page_mask)
        self.l.left.credits_page_mask = ImageTk.PhotoImage(image=credits_page_mask)
        self.l.right.credits_page_mask = ImageTk.PhotoImage(image=credits_page_mask.rotate(180))

        dialogue_box_mask = Image.open(self.dialogue_box)
        self.l.left.dialogue_box_mask = ImageTk.PhotoImage(image=dialogue_box_mask)
        self.l.right.dialogue_box_mask = ImageTk.PhotoImage(image=dialogue_box_mask.rotate(180))

        for countdown_index, step_image_path in enumerate(self.countdown_images):
            countdown_image = Image.open(step_image_path)
            self.l.left.countdown_images.append(ImageTk.PhotoImage(image=countdown_image))
            self.l.right.countdown_images.append(ImageTk.PhotoImage(image=countdown_image.rotate(180)))
