# Software development setup

## Install on Ubuntu

### Install Python
To run the app, you first need to install Python 
To check if you have python, run in your terminal : 
```
python --version
```
or
```
python3 --version
```
If there is no python, or if it's a version prior to 3.8, run :
```
sudo add-apt-repository ppa:deadsnakes/ppa
sudo apt install python3.8
sudo apt install python3-pip
```
In your ~/.bashrc file, add the following lines :
```
alias python=python3.8
alias pip=pip3
```
then run the following command to update the bashrc file :
```
source ~/.bashrc
```

### Tkinter and Pillow
Run the following lines in your terminal :
```
sudo apt install python-tk
sudo apt install python3-pil python3-pil.imagetk
```

## VLC
If you do not have VLC installed on your computer, please run :
```
sudo apt install vlc
```
If later in the application, the audio is missing from the AI explanation videos, please open your VLC settings and change the audio output type.

## Install on Windows 10

### Install Python 3.8.5
To run the app, you first need to install Python 
Download and install python-3.8.5-amd64.exe from https://www.python.org/downloads/
Do not forget to enable file association & Environnement variables

### VLC
Download and install VLC 3.0:
https://www.videolan.org/vlc/download-windows.fr.html

### Phidgets control
Download and install Phidgets drivers 1.6 for Windows 10 64bits
https://www.phidgets.com/docs/Phidgets_Drivers
or Phidget22-x64_1.6.20200921 - https://www.phidgets.com/downloads/phidget22/libraries/windows/Phidget22-x64.exe

## NVidia drivers for better performance

To improve prediction execution speed, you can use an NVidia graphics card and the CUDA toolkit.

See [Improve performance](AI_PERF_CUDA.md) page.