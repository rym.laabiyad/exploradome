# <span style="font-family: Arial Black;">Trangr<span style="color: #fdc441;">I</span><span style="color: #f97738;">A</span>m</span>
<span style="font-family: Arial Black;">Trangr<span style="color: #fdc441;">I</span><span style="color: #f97738;">A</span>m</span> is an educational project to understand how Artificial Intelligence works.

The player (museum visitor) will be ask to solve a Tangram puzzle. While solving the puzzle, an computer vision based AI will predict the tangram gigure the user is doing.

![TrangrIAm gameplay](docs/tangriam-gameplay.gif)

More information on this page : https://blog.octo.com/tangriam-project-comment-expliquer-l-ia-aux-enfants/

This project is owned by [Exploradome](http://www.exploradome.fr).

It has been developed by [OCTO Technology](https://www.octo.com) with the support of [Microsoft](https://www.microsoft.com).

# Hardware requirements
- Intel i7 CPU
- RAM 8GB or 16GB
- Harddrive/SSD: 2GB of free space
- Integrated graphics card or NVidia Geforce (recommanded)
- 1080p projector
- Phidgets buttons (optional)

# OS requirements
- Windows 10 or Ubuntu (may work on other Linux distributions)

# Software development setup

See [installation instructions](INSTALL.md).

# <span style="font-family: Arial Black;">Trangr<span style="color: #fdc441;">I</span><span style="color: #f97738;">A</span>m</span> Computer Vision workflow

![TrangrIAm Computer Vision workflow](docs/tangriam-process-ia.gif)


# Start TangrIAm app

If you're using Windows, unzip TangrIAm app somewhere on the C: Drive (ex: c:\tangrIAm), then to run the app, double-click on ```c:\tangrIAm\TangrIAm.bat``` 

If you're non-Windows OS, go to the project folder, run ```pip install -r requirements.txt```, then go to the src file and run python main.py

If it doesn't start, carefully read the error messages.
For example, you might be using the wrong camera id (check the config.ini file to change that)

# Keyboard shortcuts for testing and calibration

See [Shortcuts and Configuration](CONFIG.md) page.

# Improve AI performance

See [Improve performance](AI_PERF_CUDA.md) page.